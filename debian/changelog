ruby-setup (3.4.1-9co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 22 Feb 2021 02:12:38 +0000

ruby-setup (3.4.1-9) unstable; urgency=medium

  * Team upload.
  * Release to unstable, incl. fix for RbConfig. (Closes: #784210)

 -- Christian Hofstaedtler <zeha@debian.org>  Mon, 04 May 2015 12:47:14 +0200

ruby-setup (3.4.1-8) experimental; urgency=medium

  * Team upload.
  * Target experimental and build with Ruby 2.2
  * Use RbConfig instead of obsolete Config for Ruby 2.2
  * Bump Standards-Version to 3.9.6 (no further changes)
  * Update Vcs-Browser to cgit URL and HTTPS
  * Drop obsolete transitional package libsetup-ruby1.8

 -- Christian Hofstaedtler <zeha@debian.org>  Thu, 09 Apr 2015 18:02:36 +0200

ruby-setup (3.4.1-7) unstable; urgency=low

  * Team upload

  [ Vincent Fourmond ]
  * Move dummy package to oldlibs

  [ Cédric Boutillier ]
  * debian/control:
    + use canonical URI in Vcs-* fields
    + bump Standards-Version to 3.9.4 (no changes needed)
    + add Breaks/Replaces/Provides for older versions of libsetup-ruby1.8
    + change priority of dummy package to extra
  * debian/copyright: convert to copyright-format/1.0
  * debian/patches: add no_makeparams.patch to not include inexistant
    $(HOME)/.makeparams, fixing FTBFS (Closes: #724224)

 -- Cédric Boutillier <boutil@debian.org>  Thu, 17 Oct 2013 16:40:45 +0200

ruby-setup (3.4.1-5) unstable; urgency=low

  [ Gunnar Wolf ]
  * Changed section to Ruby as per ftp-masters' request
  * Added myself as an uploader
  
  [ Lucas Nussbaum ]
  * Removing Lucas from uploaders.

  [ Vincent Fourmond ]
  * Rename package as ruby-setup
  * Use source format 3.0 (quilt)
    - drop dependency on dpacth 
    - convert old patches
  * Use gem2deb for building
  * Provide a libsetup-ruby1.8 transition package
  * Conforms to standards 3.9.2, and to the newer ruby policy

 -- Vincent Fourmond <fourmond@debian.org>  Mon, 30 May 2011 14:15:58 +0200

libsetup-ruby (3.4.1-4) unstable; urgency=low

  * Conforms to Standards 3.7.3
  * Cleanup doc-base/libsetup-ruby

 -- Vincent Fourmond <fourmond@debian.org>  Sun, 20 Apr 2008 16:06:04 +0200

libsetup-ruby (3.4.1-3) unstable; urgency=low

  * Added debian/patches/fix-metaconfig.dpatch.
    Fix add_bool_config in metaconfig. From upstream SVN r2634.
  * debian/control: added myself to uploaders
  * debian/control: fixed Vcs-* to point to trunk/

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Wed, 13 Feb 2008 23:53:48 +0100

libsetup-ruby (3.4.1-2) unstable; urgency=low

  * Fix the copyright problems...

 -- Vincent Fourmond <fourmond@debian.org>  Mon, 03 Dec 2007 18:43:08 +0100

libsetup-ruby (3.4.1-1) unstable; urgency=low

  * Initial release (no ITP filed...)

 -- Vincent Fourmond <fourmond@debian.org>  Mon, 26 Nov 2007 21:41:33 +0100
